//
// Created by Karl Meinkopf on 05.07.2021.
//

#include "CodecTagString.h"

#include <libavutil/avutil.h>

const char *codecTagToString(int32_t tag) {
	char *buf = malloc(AV_FOURCC_MAX_STRING_SIZE * sizeof(char));

	av_fourcc_make_string(buf, tag);

	return buf;
}
