//
// Created by Karl Meinkopf on 05.07.2021.
//

#ifndef DARTAVCONV_CODECTAGSTRING_H
#define DARTAVCONV_CODECTAGSTRING_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

const char *codecTagToString(int32_t tag);

#ifdef __cplusplus
};
#endif

#endif //DARTAVCONV_CODECTAGSTRING_H
