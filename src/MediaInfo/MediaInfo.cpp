//
// Created by Дом on 22.07.2021.
//

#include "MediaInfo.h"

VideoInfos VideoInfos_fromVector(const std::vector<VideoInfo> &vec) {
	auto ret = VideoInfos();

	ret.size = vec.size();
	ret.items = new VideoInfo[ret.size];

	for(auto i = 0; i < ret.size; i++) {
		ret.items[i] = vec[i];
	}

	return ret;
}

AudioInfos AudioInfos_fromVector(const std::vector<AudioInfo> &vec) {
	auto ret = AudioInfos();

	ret.size = vec.size();
	ret.items = new AudioInfo[ret.size];

	for(auto i = 0; i < ret.size; i++) {
		ret.items[i] = vec[i];
	}

	return ret;
}

void CDEF_EXPORT_MediaInfo_dispose(MediaInfo info) {
	delete[] info.container.filename;
	delete[] info.container.format;

	for(int i = 0; i < info.audio.size; i++) {
		delete[] info.audio.items[i].codec.name;
		delete[] info.audio.items[i].codec.tag;
	}

	for(int i = 0; i < info.video.size; i++) {
		delete[] info.video.items[i].codec.name;
		delete[] info.video.items[i].codec.tag;
	}

	delete[] info.audio.items;
	delete[] info.video.items;
}
