#ifndef DARTAVCONV_MEDIAINFO_H
#define DARTAVCONV_MEDIAINFO_H

#ifdef __cplusplus

#include <cstdint>
#include <vector>

#else
#include <stdint.h>
#endif

#include "../macro.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	IS_OK,
	IS_NO_INPUT_FILE,
	IS_NO_INPUT_STREAMS,
} CDEF_EXPORT_InfoStatus;

typedef struct {
	char *name;
	char *tag;
} Codec;

typedef struct {
	int64_t bitrate;
	uint64_t duration;
	Codec codec;

	double sampleRate;
} AudioInfo;

typedef struct {
	uint32_t width;
	uint32_t height;
} Resolution;

typedef struct {
	int64_t bitrate;
	uint64_t duration;
	Codec codec;

	char *pixFormat;
	double fps;
	Resolution resolution;
} VideoInfo;

typedef struct {
	uint64_t duration;
	uint64_t fileSize;
	char *filename;
	char *format;
} ContainerInfo;

C_LIST(VideoInfos, VideoInfo)
C_LIST(AudioInfos, AudioInfo)

typedef struct {
	ContainerInfo container;

	VideoInfos video;
	AudioInfos audio;

	CDEF_EXPORT_InfoStatus status;
} MediaInfo;

void CDEF_EXPORT_MediaInfo_dispose(MediaInfo info);

#ifdef __cplusplus
VideoInfos VideoInfos_fromVector(const std::vector<VideoInfo> &);
AudioInfos AudioInfos_fromVector(const std::vector<AudioInfo> &);
#endif

#ifdef __cplusplus
};
#endif

#endif //DARTAVCONV_MEDIAINFO_H
