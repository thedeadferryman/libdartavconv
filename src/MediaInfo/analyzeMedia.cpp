//
// Created by Karl Meinkopf on 02.07.2021.
//

#include "analyzeMedia.h"
#include "CodecTagString.h"

extern "C" {
#include <libavformat/avformat.h>
#include <libavutil/pixdesc.h>
}

#include <filesystem>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#define ANALYZE_CODEC(codecOut, codecPar) { \
    const char *INT__codecTag__INT = codecTagToString((codecPar)->codec_tag); \
    WRITE_STRING((codecOut).name, avcodec_get_name((codecPar)->codec_id)) \
    WRITE_STRING((codecOut).tag, INT__codecTag__INT) \
    delete INT__codecTag__INT; \
}

using namespace std;
namespace FS = std::filesystem;

VideoInfo analyzeVideoStream(AVStream *stream) {
	auto videoInfo = VideoInfo();

	auto pixFmtName = av_get_pix_fmt_name(
			static_cast<AVPixelFormat>(stream->codecpar->format));

	videoInfo.duration = AVR_TO_TIME(stream->duration, stream->time_base);
	videoInfo.bitrate = stream->codecpar->bit_rate;
	videoInfo.fps = AVR_VALUE(stream->avg_frame_rate);
	WRITE_STRING(videoInfo.pixFormat, pixFmtName)

	videoInfo.codec = {};
	ANALYZE_CODEC(videoInfo.codec, stream->codecpar)

	videoInfo.resolution = {};
	videoInfo.resolution.width = stream->codecpar->width;
	videoInfo.resolution.height = stream->codecpar->height;

	return videoInfo;
}

AudioInfo analyzeAudioStream(AVStream *stream) {
	auto audioInfo = AudioInfo();

	audioInfo.bitrate = stream->codecpar->bit_rate;
	audioInfo.duration = AVR_TO_TIME(stream->duration, stream->time_base);
	audioInfo.sampleRate = stream->codecpar->sample_rate;

	audioInfo.codec = {};
	ANALYZE_CODEC(audioInfo.codec, stream->codecpar)

	return audioInfo;
}

string getContainerFormat(AVFormatContext *ctx) {
	auto longName = ctx->iformat->long_name;

	auto entry = av_dict_get(ctx->metadata, "major_brand", nullptr, 0);

	auto major = entry ? entry->value : nullptr;

	stringstream ss;

	if (major) {
		ss << major;

		if (longName) {
			ss << '(' << longName << ')';
		}
	} else {
		if (longName) {
			ss << longName;
		} else {
			ss << "unknown";
		}
	}

	return ss.str();
}

MediaInfo CDEF_EXPORT_analyzeMedia(const char *filepath) {
	auto info = MediaInfo();

	auto fmtCtx = avformat_alloc_context();

	auto ret = avformat_open_input(&fmtCtx, filepath, nullptr, nullptr);

	if (ret != 0) {
		info.status = CDEF_EXPORT_InfoStatus::IS_NO_INPUT_FILE;
		return info;
	}

	const filesystem::path &fpath = FS::path(fmtCtx->url);

	info.container = {};

	info.container.duration = AVR_TO_TIME(fmtCtx->duration, AV_TIME_BASE_Q);
	WRITE_STRING(info.container.filename, fpath.filename().generic_string().c_str())
	info.container.fileSize = FS::file_size(fpath);
	WRITE_STRING(info.container.format, getContainerFormat(fmtCtx).c_str())

	ret = avformat_find_stream_info(fmtCtx, nullptr);

	if (ret != 0) {
		info.status = CDEF_EXPORT_InfoStatus::IS_NO_INPUT_STREAMS;
		return info;
	}

	vector<VideoInfo> videoInfos;
	vector<AudioInfo> audioInfos;

	for (size_t i = 0; i < fmtCtx->nb_streams; i++) {
		switch (fmtCtx->streams[i]->codecpar->codec_type) {
		case AVMEDIA_TYPE_VIDEO:
			videoInfos.push_back(analyzeVideoStream(fmtCtx->streams[i]));
			break;
		case AVMEDIA_TYPE_AUDIO:
			audioInfos.push_back(analyzeAudioStream(fmtCtx->streams[i]));
			break;
		default:
			break;
		}
	}

	info.audio = AudioInfos_fromVector(audioInfos);
	info.video = VideoInfos_fromVector(videoInfos);

	avformat_close_input(&fmtCtx);

	info.status = CDEF_EXPORT_InfoStatus::IS_OK;

	return info;
}
