//
// Created by Karl Meinkopf on 02.07.2021.
//

#ifndef DARTAVCONV_ANALYZEMEDIA_H
#define DARTAVCONV_ANALYZEMEDIA_H

#include "MediaInfo.h"

#ifdef __cplusplus
extern "C" {
#endif

MediaInfo CDEF_EXPORT_analyzeMedia(const char *filepath);

#ifdef __cplusplus
};
#endif

#endif //DARTAVCONV_ANALYZEMEDIA_H
