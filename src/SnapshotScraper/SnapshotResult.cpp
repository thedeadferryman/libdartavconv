//
// Created by Дом on 22.07.2021.
//

#include "SnapshotResult.h"

Snapshots Snapshots_fromVector(const std::vector<Snapshot> &vec) {
	auto ret = Snapshots();

	ret.size = vec.size();
	ret.items = new Snapshot[ret.size];

	for(auto i = 0; i < ret.size; i++) {
		ret.items[i] = vec[i];
	}

	return ret;
}

void CDEF_EXPORT_SnapshotResult_dispose(SnapshotResult result) {
	delete[] result.snaps.items;
}
