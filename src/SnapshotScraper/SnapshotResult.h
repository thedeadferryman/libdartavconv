//
// Created by Дом on 22.07.2021.
//

#ifndef DARTAVCONV_SNAPSHOTRESULT_H
#define DARTAVCONV_SNAPSHOTRESULT_H

#ifdef __cplusplus

#include <cstdint>
#include <vector>

#else
#include <stdint.h>
#endif

#include "../macro.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	SS_OK,
	SS_NO_INPUT_FILE,
	SS_NO_VIDEO_STREAM,
	SS_ERR_OUTPUT_FILE,
	SS_ERR_WRITE_FRAME,
	SS_ERR_WRITE_HEADER,
	SS_ERR_WRITE_TRAILER,
	SS_ERR_OUT_OF_RANGE,
} CDEF_EXPORT_SnapshotStatus;

typedef struct {
	CDEF_EXPORT_SnapshotStatus status;
	char *path;
	int64_t timecode;
} Snapshot;

C_LIST(Snapshots, Snapshot)

typedef struct {
	CDEF_EXPORT_SnapshotStatus status;
	Snapshots snaps;
} SnapshotResult;

void CDEF_EXPORT_SnapshotResult_dispose(SnapshotResult result);

#ifdef __cplusplus
Snapshots Snapshots_fromVector(const std::vector<Snapshot> &);
#endif

#ifdef __cplusplus
}
#endif

#endif //DARTAVCONV_SNAPSHOTRESULT_H
