//
// Created by Karl Meinkopf on 04.07.2021.
//

#include "SnapshotScraper.h"

extern "C" {
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
}

#include <iostream>
#include <sstream>
#include <string>
#include <filesystem>

using namespace std;
using std::filesystem::path;

CDEF_EXPORT_SnapshotStatus
handleSnapFrame(const char *outFile, AVFrame *frame, AVCodecContext *inCodecCtx) {
	AVFormatContext *outFmtCtx;

	avformat_alloc_output_context2(&outFmtCtx, nullptr, nullptr, outFile);

	auto outCodec = avcodec_find_encoder(IMAGE_OUTPUT_CODEC);
	auto outCodecCtx = avcodec_alloc_context3(outCodec);

	auto outStream = avformat_new_stream(outFmtCtx, outCodec);

	outCodecCtx->width = frame->width;
	outCodecCtx->height = frame->height;
	outCodecCtx->pix_fmt = outCodec->pix_fmts[0];

	outCodecCtx->time_base = AV_TIME_BASE_Q;

	avcodec_open2(outCodecCtx, outCodec, nullptr);

	auto swsCtx = sws_getContext(inCodecCtx->width, inCodecCtx->height, inCodecCtx->pix_fmt,
	                             outCodecCtx->width, outCodecCtx->height, outCodecCtx->pix_fmt,
	                             SWS_INTERP_TYPE, nullptr, nullptr, nullptr);


	auto outFrame = av_frame_alloc();

	outFrame->width = outCodecCtx->width;
	outFrame->height = outCodecCtx->height;
	outFrame->format = outCodecCtx->pix_fmt;

	av_image_alloc(outFrame->data, outFrame->linesize, outCodecCtx->width, outCodecCtx->height, outCodecCtx->pix_fmt,
	               32);

	sws_scale(swsCtx, frame->data, frame->linesize, 0, inCodecCtx->height, outFrame->data, outFrame->linesize);

	auto ret = avio_open(&outFmtCtx->pb, outFile, AVIO_FLAG_WRITE);

	if (ret < 0) {
		avio_close(outFmtCtx->pb);
		avformat_free_context(outFmtCtx);
		avcodec_free_context(&outCodecCtx);

		av_freep(&outFrame->data[0]);
		av_freep(&outCodec);
		av_frame_free(&outFrame);

		sws_freeContext(swsCtx);

		return CDEF_EXPORT_SnapshotStatus::SS_ERR_OUTPUT_FILE;
	}

	ret = avformat_write_header(outFmtCtx, nullptr);

	if (ret < 0) {
		avio_close(outFmtCtx->pb);
		avformat_free_context(outFmtCtx);
		avcodec_free_context(&outCodecCtx);

		av_freep(&outFrame->data[0]);
		av_freep(&outCodec);
		av_frame_free(&outFrame);

		sws_freeContext(swsCtx);

		return CDEF_EXPORT_SnapshotStatus::SS_ERR_WRITE_HEADER;
	}

	auto outPacket = av_packet_alloc();

	avcodec_send_frame(outCodecCtx, outFrame);
	avcodec_receive_packet(outCodecCtx, outPacket);

	outPacket->stream_index = outStream->index;

	av_packet_rescale_ts(outPacket, inCodecCtx->time_base, outCodecCtx->time_base);

	ret = av_interleaved_write_frame(outFmtCtx, outPacket);

	if (ret < 0) {
		avio_close(outFmtCtx->pb);
		avformat_free_context(outFmtCtx);
		avcodec_free_context(&outCodecCtx);

		av_freep(&outFrame->data[0]);
		av_freep(&outCodec);
		av_frame_free(&outFrame);
		av_packet_free(&outPacket);

		sws_freeContext(swsCtx);

		return CDEF_EXPORT_SnapshotStatus::SS_ERR_WRITE_FRAME;
	}

	ret = av_write_trailer(outFmtCtx);

	if (ret < 0) {
		avio_close(outFmtCtx->pb);
		avformat_free_context(outFmtCtx);
		avcodec_free_context(&outCodecCtx);

		av_freep(&outFrame->data[0]);
		av_freep(&outCodec);
		av_frame_free(&outFrame);
		av_packet_free(&outPacket);

		sws_freeContext(swsCtx);

		return CDEF_EXPORT_SnapshotStatus::SS_ERR_WRITE_TRAILER;
	}

	avio_close(outFmtCtx->pb);
	avformat_free_context(outFmtCtx);
	avcodec_free_context(&outCodecCtx);

	av_freep(&outFrame->data[0]);
	av_freep(&outCodec);
	av_frame_free(&outFrame);
	av_packet_free(&outPacket);

	sws_freeContext(swsCtx);

	return CDEF_EXPORT_SnapshotStatus::SS_OK;
}

string frameFilename(const char *outDir, int64_t ts) {
	return (path(outDir) / path((stringstream() << OUT_FILE_TEMPLATE).str())).generic_string();
}

SnapshotResult
CDEF_EXPORT_collectSnapshots(const char *inFile, const char *outDir, size_t count,
                             FNTYPE(void, onProgress, size_t, size_t)) {
	av_log_set_level(AV_LOG_ERROR);

	auto fmtCtx = avformat_alloc_context();

	auto ret = avformat_open_input(&fmtCtx, inFile, nullptr, nullptr);

	if (ret != 0) {
		avformat_close_input(&fmtCtx);

		return { .status = CDEF_EXPORT_SnapshotStatus::SS_NO_INPUT_FILE };
	}

	avformat_find_stream_info(fmtCtx, nullptr);

	auto streamId = av_find_best_stream(fmtCtx, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr, 0);

	if (streamId < 0) {
		avformat_close_input(&fmtCtx);

		return { .status = CDEF_EXPORT_SnapshotStatus::SS_NO_VIDEO_STREAM };
	}

	auto stream = fmtCtx->streams[streamId];

	auto codecPar = stream->codecpar;
	auto codec = avcodec_find_decoder(codecPar->codec_id);
	auto codecCtx = avcodec_alloc_context3(codec);

	avcodec_parameters_to_context(codecCtx, codecPar);
	avcodec_open2(codecCtx, codec, nullptr);

	auto step = round(static_cast<double>(stream->duration) / static_cast<double>(count));

	onProgress(0, count);

	vector<Snapshot> snaps;

	for (size_t i = 0; i < count; i++) {
		auto curTs = static_cast<int64_t>(round(step * (static_cast<double>(i) + 0.5)));

		av_seek_frame(fmtCtx, streamId, curTs, 0);

#pragma unroll
		while (true) {
			auto packet = av_packet_alloc();

			auto rret = av_read_frame(fmtCtx, packet);
			if (rret < 0) {
				av_packet_free(&packet);
				break;
			}

			if (packet->stream_index != streamId) {
				av_packet_free(&packet);
				continue;
			}

			auto frame = av_frame_alloc();

			avcodec_send_packet(codecCtx, packet);
			avcodec_receive_frame(codecCtx, frame);

			av_packet_free(&packet);

			if (frame->pts < 0) {
				av_frame_free(&frame);
				continue;
			}
			if (!frame->key_frame) {
				av_frame_free(&frame);
				continue;
			}

			auto filename = frameFilename(outDir, frame->pts);

			auto res = handleSnapFrame(filename.c_str(),
			                           frame, codecCtx);

			if (res != CDEF_EXPORT_SnapshotStatus::SS_OK) {
				avformat_close_input(&fmtCtx);
				avcodec_free_context(&codecCtx);
				av_frame_free(&frame);

				return { .status = res };
			}

			onProgress(i + 1, count);

			auto snap = Snapshot {
					.status = CDEF_EXPORT_SnapshotStatus::SS_OK,
					.path = new char[1],
					.timecode = AVR_TO_TIME(frame->pts, stream->time_base),
			};

			WRITE_STRING(snap.path, filename.c_str())

			snaps.emplace_back(snap);

			av_frame_free(&frame);

			break;
		}
	}

	avformat_close_input(&fmtCtx);
	avcodec_free_context(&codecCtx);

	return {
			.status = CDEF_EXPORT_SnapshotStatus::SS_OK,
			.snaps = Snapshots_fromVector(snaps)
	};
}

Snapshot CDEF_EXPORT_takePreciseSnapshot(const char *inFile, uint64_t timecode, const char *outFile) {
	av_log_set_level(AV_LOG_FATAL);

	auto fmtCtx = avformat_alloc_context();

	auto ret = avformat_open_input(&fmtCtx, inFile, nullptr, nullptr);

	if (ret != 0) {
		avformat_close_input(&fmtCtx);

		return { .status = CDEF_EXPORT_SnapshotStatus::SS_NO_INPUT_FILE };
	}

	avformat_find_stream_info(fmtCtx, nullptr);

	auto streamId = av_find_best_stream(fmtCtx, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr, 0);

	if (streamId < 0) {
		avformat_close_input(&fmtCtx);

		return { .status = CDEF_EXPORT_SnapshotStatus::SS_NO_VIDEO_STREAM };
	}

	auto stream = fmtCtx->streams[streamId];

	auto ts = TIME_TO_AVR(timecode, stream->time_base);

	if (ts >= stream->duration) {
		avformat_close_input(&fmtCtx);

		return { .status = CDEF_EXPORT_SnapshotStatus::SS_ERR_OUT_OF_RANGE };
	}

	auto codecPar = stream->codecpar;
	auto codec = avcodec_find_decoder(codecPar->codec_id);
	auto codecCtx = avcodec_alloc_context3(codec);

	avcodec_parameters_to_context(codecCtx, codecPar);
	avcodec_open2(codecCtx, codec, nullptr);

	av_seek_frame(fmtCtx, streamId, ts, AVSEEK_FLAG_BACKWARD);

	auto packet = av_packet_alloc();
#pragma unroll
	while (true) {
		auto rret = av_read_frame(fmtCtx, packet);
		if (rret < 0) break;

		if (packet->stream_index != streamId) continue;

		auto frame = av_frame_alloc();

		avcodec_send_packet(codecCtx, packet);
		avcodec_receive_frame(codecCtx, frame);

		av_packet_unref(packet);

		if (frame->pts < 0) continue;

		if (frame->pts >= ts) {
			auto res = handleSnapFrame(outFile,
			                           frame, codecCtx);

			av_frame_free(&frame);

			if (res != CDEF_EXPORT_SnapshotStatus::SS_OK) {
				return { .status = res };
			} else {
				auto snap = Snapshot {
						.status = CDEF_EXPORT_SnapshotStatus::SS_OK,
						.path = new char[1],
						.timecode = AVR_TO_TIME(frame->pts, stream->time_base)
				};

				WRITE_STRING(snap.path, outFile)

				return snap;
			}
		}

		av_frame_free(&frame);

		break;
	}

	return { .status = CDEF_EXPORT_SnapshotStatus::SS_ERR_OUT_OF_RANGE };
}
