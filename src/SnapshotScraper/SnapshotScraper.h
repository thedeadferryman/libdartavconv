//
// Created by Karl Meinkopf on 04.07.2021.
//

#ifndef DARTAVCONV_SNAPSHOTSCRAPER_H
#define DARTAVCONV_SNAPSHOTSCRAPER_H

#include "config.h"
#include "SnapshotResult.h"

#ifdef __cplusplus
extern "C" {
#endif

SnapshotResult CDEF_EXPORT_collectSnapshots(const char *inFile, const char *outDir,
                                            size_t count,
                                            FNTYPE(void, onProgress, size_t, size_t));

Snapshot CDEF_EXPORT_takePreciseSnapshot(const char *inFile, uint64_t timecode, const char *outFile);

#ifdef __cplusplus
}
#endif

#endif //DARTAVCONV_SNAPSHOTSCRAPER_H
