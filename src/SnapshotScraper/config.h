//
// Created by Дом on 10.07.2021.
//

#ifndef DARTAVCONV_CONFIG_H
#define DARTAVCONV_CONFIG_H

#define IMAGE_OUTPUT_CODEC AV_CODEC_ID_PNG
#define SWS_INTERP_TYPE SWS_BICUBIC
#define OUT_FILE_TEMPLATE "frame" << ts << ".png"

#endif //DARTAVCONV_CONFIG_H
