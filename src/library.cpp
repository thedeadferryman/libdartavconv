#include "library.h"

#include <iostream>

extern "C" {
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
};

using namespace std;

void hello() {
	auto fmtCtx = avformat_alloc_context();

	avformat_open_input(&fmtCtx, "..\\sample.mp4", nullptr, nullptr);

	avformat_find_stream_info(fmtCtx, nullptr);

	uint32_t streamId = av_find_best_stream(fmtCtx, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr, 0);

	if(streamId < 0) {
		cerr << "no video stream" << endl;
		return;
	}

	AVCodecParameters *codecPar = fmtCtx->streams[streamId]->codecpar;
	AVCodec *codec = avcodec_find_decoder(codecPar->codec_id);


	auto codecCtx = avcodec_alloc_context3(codec);
	avcodec_parameters_to_context(codecCtx, codecPar);
	avcodec_open2(codecCtx, codec, nullptr);

	auto frame = av_frame_alloc();
	auto packet = av_packet_alloc();

	av_seek_frame(fmtCtx, static_cast<int32_t>(streamId), 5, 0);

	while(true) {
		if(av_read_frame(fmtCtx, packet) < 0) break;

		if(packet->stream_index == streamId) break;
	}

	avcodec_send_packet(codecCtx, packet);
	avcodec_receive_frame(codecCtx, frame);

	AVFormatContext *outFmtCtx;

	std::string fout = "..\\sample.png";

	avformat_alloc_output_context2(&outFmtCtx, nullptr, nullptr, fout.c_str());

	auto outCodec = avcodec_find_encoder(AV_CODEC_ID_PNG);
	auto outCodecCtx = avcodec_alloc_context3(outCodec);

	auto outStream = avformat_new_stream(outFmtCtx, outCodec);

	outCodecCtx->width = frame->width;
	outCodecCtx->height = frame->height;
	outCodecCtx->pix_fmt = outCodec->pix_fmts[0];

	outCodecCtx->time_base = av_inv_q(codecCtx->framerate);

	avcodec_open2(outCodecCtx, outCodec, nullptr);

	auto swsCtx = sws_getContext(codecCtx->width, codecCtx->height, codecCtx->pix_fmt,
	                             outCodecCtx->width, outCodecCtx->height, outCodecCtx->pix_fmt,
	                             SWS_BICUBIC, nullptr, nullptr, nullptr);

	auto outFrame = av_frame_alloc();

	outFrame->width = outCodecCtx->width;
	outFrame->height = outCodecCtx->height;
	outFrame->format = outCodecCtx->pix_fmt;

	av_image_alloc(outFrame->data, outFrame->linesize, outCodecCtx->width, outCodecCtx->height, outCodecCtx->pix_fmt, 32);

	sws_scale(swsCtx, frame->data, frame->linesize, 0, codecCtx->height, outFrame->data, outFrame->linesize);

	int32_t ret = avio_open(&outFmtCtx->pb, fout.c_str(), AVIO_FLAG_WRITE);

	if(ret < 0) {
		cerr << "file no open out" << endl;
	}

	ret = avformat_write_header(outFmtCtx, nullptr);

	if(ret < 0) {
		cerr << "file no write out" << endl;
	}

	auto outPacket = av_packet_alloc();

	avcodec_send_frame(outCodecCtx, outFrame);
	avcodec_receive_packet(outCodecCtx, outPacket);

	outPacket->stream_index = outStream->index;

	av_packet_rescale_ts(outPacket, codecCtx->time_base, outCodecCtx->time_base);

	cout << av_interleaved_write_frame(outFmtCtx, outPacket) << endl;

	cout << av_write_trailer(outFmtCtx) << endl;

	avio_close(outFmtCtx->pb);
}
