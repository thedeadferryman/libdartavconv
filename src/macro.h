//
// Created by Karl Meinkopf on 05.07.2021.
//

#ifndef DARTAVCONV_MACRO_H
#define DARTAVCONV_MACRO_H

#define AVR_VALUE(avr) (static_cast<double>((avr).num) / (avr).den)
#define AVR_TO_TIME(dur, rat) av_rescale((dur), (rat).num, (rat).den)
#define TIME_TO_AVR(dur, rat) av_rescale((dur), (rat).den, (rat).num)

#define NOT_NULL(ptr) ((ptr) != nullptr)

#define FNTYPE(return_t, name, ...) return_t (* name)(__VA_ARGS__)
#define C_LIST(name, member_t) typedef struct { member_t* items; size_t size; } name;

#ifdef __cplusplus
#define PTRCAST(a, type) reinterpret_cast<type*>(a)
#else
#define PTRCAST(a, type) (type*)(a)
#endif
#ifdef __cplusplus
#define WRITE_STRING(dest, src) { \
    delete[] (dest);                   \
    uint64_t len = strlen(src) + 1; \
    dest = PTRCAST(malloc(sizeof(char) * len), char); \
    memset(dest, 0, sizeof(char) * len); \
    strcpy(dest, src); \
}
#endif

#endif //DARTAVCONV_MACRO_H
